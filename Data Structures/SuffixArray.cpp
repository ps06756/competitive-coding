#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class SuffixArray
{
	public:
		string str;
		vector<array<int,3> > vec;
		vector<int> suff, sorted;
		vector<string> sortedString;
		SuffixArray(string st)
		{
			str = st + "$";
			vec = vector<array<int,3> >(SZ(str));
			suff = vector<int> (SZ(str)-1);
			sorted = vector<int> (SZ(str)-1);
			sortedString = vector<string>(SZ(str)-1);
		}
		void construct()
		{

			vector<vector<int> > dp = vector<vector<int> >(ceil(log2(SZ(str))) + 1, vector<int> (SZ(str),-1));
			vector<pair<string,int > > temp;
			REP(i, SZ(str))
				temp.pb(make_pair(str.substr(i,1), i));
			sort(all(temp));
			map<string,int> mp;
			int index = 0;
			foreach(temp, it)
			{
				string st = it->first;
				if (mp.count(st) == 0)
					mp[st] = index++;
			}
			foreach(temp, it)
				dp[0][it->second] = mp[it->first];
			int step = 0;
			for(step = 0; (1 << step) < SZ(str); step++)
			{
				for(int i = 0; i < SZ(str); i++)
				{
					if (i + (1 << step) < SZ(str))
						vec[i] = { dp[step][i], dp[step][i + (1 << step)], i };
					else
						vec[i] = { dp[step][i], -1, i };
				}
				sort(all(vec));
				index = 0;
				mp.clear();
				for(array<int,3>& arr : vec)
				{	
					string st2 = str.substr(arr[2], min(1 << (step+1), SZ(str) - arr[2] - 1));
					if (mp.find(st2) == mp.end())
						mp[st2] = index++;
				}
				for(array<int,3> & arr : vec)
				{
					string st2 = str.substr(arr[2], min(1 << (step+1), SZ(str) - arr[2] - 1));
					dp[step+1][arr[2]] = mp[st2];
				}
			}
			for(int i = 0; i < SZ(str)-1; i++)
			{
				suff[i] = dp[step][i] - 1;
				sorted[dp[step][i]-1] = i;
				string st = str.substr(i); st.pop_back();
				sortedString[dp[step][i]-1] = st;
			}

		}
};
int main()
{
	string str; cin >> str;
	SuffixArray suff(str);
	suff.construct();
	for(string st : suff.sortedString)
		cout << st << "\n";
	return (0);
}
