#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef vector<int> vi;
typedef map<int,int> mii;
typedef set<int> si;
typedef pair<int,int> pii;
typedef vector<pair<int,int> > vpii;

int main()
{
	ios::sync_with_stdio(false);
	int n; cin>>n;
	vi vec(n);
	multiset<int> st;
	REP(i,n)
	{
		cin>>vec[i];
		if (st.find(vec[i]) == st.end())
		{
			st.insert(vec[i]);
			auto it = st.find(vec[i]);
			it++;
			if (it != st.end())
				st.erase(it);
		}
	}
	cout<<SZ(st)<<"\n";
}
