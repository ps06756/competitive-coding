#include <iostream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
template class 	std::vector<int> ;



class DSU
{
	public:
		vector<int> rank,parent;
		DSU(int n)
		{
			rank = vector<int>(n,0);
			parent = vector<int>(n);
			REP(i,n)
				parent[i] = i;
		}
		void mergeSet(int x, int y)
		{
			int px = findSet(x);
			int py = findSet(y);
			if (rank[px] > rank[py])
				parent[py] = x;
			else
				parent[px] = py;
			if (rank[px] == rank[py])
				rank[py] += 1;
		}
		int findSet(int x)
		{
			if (x != parent[x])  parent[x] = findSet(parent[x]);
			return parent[x];
		}
};

