#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
typedef vector<int> vi;
typedef map<int,int> mii;
typedef set<int> si;
typedef pair<int,int> pii;
typedef vector<pair<int,int> > vpii;


class Tree
{
	public:
		vector<vector<int> > vec;
		Tree(int n)
		{
			vec = vector<vector<int> >(n);
		}
		void addEdge(int u, int v)
		{
			vec[u].push_back(v);
			vec[v].push_back(u);
		}
		void dfs(int source, int parent, vector<bool>& visited, vector<int>& tour, vector<int>& level)
		{
			if (visited[source] == false)
			{
				level[source] = level[parent] + 1;
				tour.push_back(source);
				visited[source] = true;
				foreach(vec[source], it)
					dfs(*it, source, visited, tour, level);
				tour.push_back(parent);
			}
		}
		void tour(int start, vector<int>& tour, vector<int>& level)
		{
			vector<bool> visited (vec.size(), false);
			dfs(start, start, visited, tour, level);
			tour.pop_back();
		}

};

class SegmentTree
{
	public:	
		vector<int> seg;
		SegmentTree(vector<int>& data)
		{
			int maxSize = (1 << (int)ceil(log2(SZ(data)) + 1));
			seg = vector<int>(maxSize + 1,0);
			initialize(data, 0, 0, SZ(data) - 1);
		}
		int initialize(vector<int>& data, int idx, int start, int end)
		{
			if (start == end)
			{
				seg[idx] = data[start];
				return seg[idx];
			}
			else
			{
				int mid = start + (end - start)/2;
				int val1 = initialize(data, 2*idx + 1, start, mid); 
				int val2 = initialize(data, 2*idx + 2, mid + 1, end); 
				seg[idx] = min(val1, val2);
				return seg[idx];
			}
			return numeric_limits<int>::max();
		}
		void update(int idx, int start, int end, int id, int val)
		{
			if (start == end)
				seg[idx] = val;
			else
			{
				int mid = start + (end - start)/2;
				if (start <= id && id <= mid)
					update(2*idx + 1, start, mid, id, val);
				else
					update(2*idx + 2, mid + 1, end, id, val);
				seg[idx] = min(seg[2*idx + 1] , seg[2*idx + 2]);

			}
		}
		pair<int, int> getMin(int idx, int start, int end, int qs, int qe)
		{
			if (start == end)
				return make_pair(seg[idx], start);
			else
			{
				int mid = start + (end - start)/2;
				if (start <= qs && qe <= mid)
					return getMin(2*idx + 1, start, mid, qs, qe);
				else if (mid + 1 <= qs && qe <= end)
					return getMin(2*idx + 2, mid + 1, end, qs, qe);
				else
				{
					pair<int, int> id1 = getMin(2*idx + 1, start, mid, qs, mid); 
					pair<int, int> id2 = getMin(2*idx + 2, mid + 1, end, mid + 1, qe);
					if (id1.first < id2.first)
						return id1;
					else
						return id2;
				}
			}
		}
};

int main()
{
	int n,m; cin>>n>>m; 
	Tree tr(n);
	for (int i = 0; i < m; i++)
	{
		int u,v;
		cin>>u>>v;
		tr.addEdge(u,v);
	}
	vector<int> level(n,0);
	vector<int> tour; 
	tr.tour(0, tour, level);
	map<int,int> first;
	REP(i, SZ(tour))
	{
		if (first.find(tour[i]) == first.end())
			first[tour[i]] = i;
	}
	vector<int> lv;
	foreach(tour, it)
		lv.push_back(level[*it] - 1);
	SegmentTree segTree(lv);
	int q; cin>>q; 
	for (int i = 0; i < q; i++)
	{
		int u, v; cin>>u>>v; 
		if (first[u] > first[v])	
			swap(u,v);
		int value = segTree.getMin(0,0,SZ(lv) - 1, first[u], first[v]).second;
		printf("Lowest common ancestor of %d and %d is %d\n", u , v , tour[value] );
	}
	return (0);
}
