#include <bits/stdc++.h>
using namespace std;

template<typename T, typename R>
class BIT
{
	public:
		vector<T> tree;
		BIT(int n)
		{
			for (int i = 0; i < n; i++)
				tree.push_back(0);
		}
		R getSum(int idx)
		{
			R cnt = 0;
			while (idx > 0)
			{
				cnt += tree[idx];
				idx -= (idx & -idx);
			}
			return cnt;
		}
		void update(int idx, T value)
		{
			while (idx <= (int)tree.size())
			{
				tree[idx] += value;
				idx += (idx & -idx);
			}
		}
};


int main()
{
	int n, q; scanf("%d%d", &n, &q);
	BIT<int, long long> bit(n);
}
