#include <iostream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

class Node
{
	public:
		int value; 
		Node* left;
		Node* right;
		Node(int val, Node* l, Node* r)
		{
			value = val;
			left = l; 
			right = r;
		}
};


class Trie
{
	public:
		Node* one, *zero;
		Trie()
		{
			one = NULL; 
			zero = NULL;
		}
		void insert(int number)
		{
			Node* node = NULL;
			if ((number >> 31) & 1)
			{
				if (one == NULL)
					one = new Node(1, NULL, NULL);
				node = one;
			}
			else
			{
				if (zero == NULL)
					zero = new Node(0,NULL,NULL);
				node = zero;
			}
			for(int i = 30; i >= 1;  i--)
			{
				if ((number >> i)&1)
				{
					if (node->right == NULL)
						node->right = new Node(1, NULL, NULL);
					node = node->right;
				}
				else
				{
					if (node->left == NULL)
						node->left = new Node(0, NULL, NULL);
					node = node->left;
				}
			}
		}
		bool find(int number)
		{
			Node* node = NULL;
			if (((number >> 31)&1) && one == NULL)
				return false;
			else if ((number >> 31) & 1)
				node = one;
			else if (((number >> 31)&1) == 0 && zero == NULL)
				return false;
			else if (((number >> 31)&1) == 0)
				node = zero;
			for(int i = 30; i >= 1;  i--)
			{
				if ((number >> i)&1)
				{
					if (node->right == NULL)
						return false;
					node = node->right;
				}
				else 
				{
					if (node->left == NULL)
						return false;
					node = node->left;
				}
			}
			return true;
		}
};

