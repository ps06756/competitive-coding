#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

i6 dp[309][309] = { } ;
i6 ans = 0;

i6 ncr(i6 n, i6 r)
{
	if (dp[n][r] > 0)
		return dp[n][r];
	else if (n == r)
		return 1;
	else if (r == 0)
		return 1;
	else if (r == 1)
		return n;
	else if (n == 0)
		return (1);
	else 
	{
		dp[n][r] = ncr(n-1,r-1) + ncr(n-1,r);
		if (dp[n][r] >= MOD)
			dp[n][r] %= MOD;
		return dp[n][r];
	}
}

void computeAnswer(int x, int y, int w, int b)
{
	 ans = ans + (((ncr(b,x)*ncr(b-x,y))%MOD)*ncr(b-x-y, w))%MOD;
	 ans  %= MOD;
}

int main()
{
	ios::sync_with_stdio(false);
	ncr(308,308);
	int tc; cin >> tc;
	while (tc--)
	{
		ans = 0;
		int r, b, w;
		cin >> r >> b >> w;
		if (r > 6*b) 
		{
			cout << "0\n" ;
			continue;
		}
		for(int i = 0; i <= w; i++)
		{
			for(int j = 0; 6*j <= r && j <= b; j++)
			{
				int k = (r - 6*j)/4;
				if (6*j + 4*k == r && j + k + i <= b)
					computeAnswer(j,k,i,b);
			}
		}
		cout << ans << "\n";
	}
	return (0);
}
