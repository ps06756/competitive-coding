#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;


pair<i6,i6> operator + (pair<i6,i6> p1, pair<i6,i6> p2)
{
	return make_pair(p1.first + p2.first, p1.second + p2.second);
}
pair<i6,i6> operator *(pair<i6,i6> p1, i6 num)
{
	return make_pair(p1.first*num,p1.second*num);
}
ostream&  operator << (ostream& os, pair<i6,i6> p)
{
	return os << p.first << " " << p.second << "\n";
}

int main()
{
	i6 q; scanf("%lld", &q);
	while (q--)
	{
		i6 n,u,v;
		scanf("%lld%lld%lld", &n, &u, &v);
		i6 parent = u;
		stack<i6> st;
		pair<i6,i6> p1 = {1,0}, p2 = {1,0};
		map<i6,i6> mp;
		while (parent != 1)
		{
			mp[parent]++;
			if (parent&1)
				parent = (parent - 1)/2;
			else
				parent /= 2;
				
		}
		parent = v;
		while (parent != 1)
		{
			mp[parent ]++;
			if (parent&1)
				parent = (parent - 1)/2;
			else
				parent /= 2;
		}
		i6 lca = 1;
		foreach(mp, it)
		{
			if (it->second == 2)
				lca = max(lca, it->first);
		}
		if (u == v)
			lca = u;
		parent = u;
		while (parent != lca)
		{
			if (parent&1)
			{
				st.push(4);
				parent = (parent - 1)/2;
			}
			else
			{
				st.push(3); parent = parent/2;
			}
		}
		while (!st.empty())
		{
			i6 top = st.top();
			st.pop();
			if (top == 3)
				p1 = p1*2;
			else 
				p1 = p1*2 + make_pair(0,1);
		}
		parent = v;
		while (parent != lca)
		{
			if (parent&1)
			{
				st.push(4);		
				parent = (parent - 1)/2;
			}
			else
			{
				st.push(3);
				parent = parent/2;
			}
		}
		while (!st.empty())
		{
			i6 top = st.top();
			st.pop();
			if (top == 3)
				p2 = p2*2;
			else
				p2 = p2*2 + make_pair(0,1);
		}
		i6 ans = min((n - p1.second)/p1.first, (n - p2.second)/p2.first);
		printf("%lld\n", ans);
	}
	return (0);
}
