#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

class matrix
{
	public:
		i6 a;
		i6 b;
		i6 c;
		i6 d;
		matrix(i6 as, i6 bs, i6 cs, i6 ds)
		{
			a = as; b = bs; c = cs; d = ds;
		}
		matrix()
		{
			a = b = c = d = 0;
		}
};

ostream& operator << (ostream& os, matrix a)
{
	return os<<a.a<<" "<<a.b<<"\n"<<a.c<<" "<<a.d<<"\n";
}

matrix operator *(matrix a, matrix b)
{
	matrix result;
	result.a = ((a.a*b.a)%MOD + (a.b*b.c)%MOD)%MOD;
	result.b = ((a.a*b.b)%MOD + (a.b*b.d)%MOD)%MOD;
	result.c = ((a.c*b.a)%MOD + (a.d*b.c)%MOD)%MOD;
	result.d = ((a.c*b.b)%MOD + (a.d*b.d)%MOD)%MOD;
	return result;
}
matrix operator +(matrix a, matrix b)
{
	matrix result;
	result.a = (a.a + b.a)%MOD;
	result.b = (a.b + b.b)%MOD;
	result.c = (a.c + b.c)%MOD;
	result.d = (a.d + b.d)%MOD;
	return result;
}

matrix operator -(matrix& a, matrix& b)
{
	matrix result;
	result.a = (a.a - b.a + MOD)%MOD;
	result.b = (a.b - b.b + MOD)%MOD;
	result.c = (a.c - b.c + MOD)%MOD;
	result.d = (a.d - b.d + MOD)%MOD;
	return result;
}

template<typename T>
class SegmentTree
{
	public:
		vector<T> tree;
		vector<T> arr;
		int64_t size;
		SegmentTree(i6 n)
		{
			size = n;
			i6 s = (1 << ((i6)ceil(log2(n))+1));
			tree = vector<T>(s);
			arr = vector<T>(s);
		}
		void constructSegmentTree(int node, int start, int end)
		{
			if (start == end)
				tree[node] = arr[start];
			else
			{
				int mid = start + (end - start)/2;
				constructSegmentTree(2*node + 1, start, mid);
				constructSegmentTree(2*node + 2, mid + 1, end);
				tree[node] = (tree[2*node + 1]*tree[2*node + 2]);
			}
		}
		void update(int l, T value)
		{
			update(0, size-1, 0, l, value);
		}
		void update(int start, int end, int index, int l,T value)
		{
			if (start == end)
			{
				tree[index] = value;
			}
			else
			{
				int mid = start + (end - start)/2;
				if (start <= l && l <= mid)
					update(start, mid, 2*index + 1, l, value);
				else if (mid + 1 <=l && l <= end)
					update(mid + 1, end, 2*index + 2, l, value);
				tree[index] = (tree[2*index + 1]*tree[2*index + 2]);
			}

		}
		T getQuery(int l, int r)
		{
			return getQuery(0, size-1, 0, l, r);
		}
		T getQuery(int start, int end, int index, int l, int r)
		{
			if (r < start || l > end)
				return matrix(1,0,0,1);
			if (l <= start && end <= r)
				return tree[index];
			int mid = start + (end - start)/2;
			T p1 = getQuery(start, mid, 2*index + 1, l, r);
			T p2 = getQuery(mid + 1, end, 2*index + 2,l,r);
			return (p1*p2);
		}
};


matrix getPow(i6 num, map<i6,matrix>& powers)
{
	if (powers.count(num) == 1)
		return powers[num];
	else
	{
		matrix a = matrix(1,1,1,0);
		if (num == 1)
			return a;
		else
		{
			matrix p = getPow(floor(num/2), powers);
			if (num%2 == 0)
				powers[num] = p*p;
			else
				powers[num] = p*p*a;
			return powers[num];
		}
	}
}

int main()
{
	map<i6,matrix> powers;
	matrix ident = matrix(1,0,0,1);
	i6 n,m; scanf("%lld %lld", &n, &m);
	SegmentTree<matrix> seg(n);
	REP(i,n)
	{
		i6 temp; scanf("%lld", &temp);
		seg.arr[i] = getPow(temp, powers) + ident;
	}
	seg.constructSegmentTree(0,0,n-1);
	REP(i,m)
	{
		char str;
		i6 l,r;
		getchar();
		str = getchar();
		scanf("%lld %lld",&l, &r);
		if (str == 'Q')
		{
			matrix result = seg.getQuery(l-1,r-1);
			result = result - ident;
			printf("%lld\n", result.b);
		}
		else
			seg.update(l-1,getPow(r, powers) + ident);
	}
	return (0);
}
