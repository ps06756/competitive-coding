#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

class Graph
{
	public:
		vector<vector<pair<int,int> > >vec;
		bool flag;
		Graph(int n)
		{
			vec = vector<vector<pair<int,int> > > (n);
			flag = false;
		}
		void addEdge(int u,int v, int x)
		{
			vec[u].pb(make_pair(v,x));
			vec[v].pb(make_pair(u,x));
		}
		void dfs(int source, int parent, vector<bool>& visited, vector<int>& color)
		{
			if (!visited[source])
			{
				visited[source] = true;
				foreach(vec[source], it)
				{
					if (it->first == parent) continue;
					if (it->second == 1)
					{
						if (!visited[it->first])
						{
							color[it->first] = !color[source];
							dfs(it->first, source, visited, color);
						}
						else if (color[it->first] != (!color[source]))
							flag = true;
					}
					else if (it->second == 0)
					{
						if (!visited[it->first])
						{
							color[it->first] = color[source];
							dfs(it->first, source, visited, color);
						}
						else if (color[source] != color[it->first])
							flag = true;
					}
				}
			}
		}
};

i6 modPow(i6 base, i6 po)
{
	i6 res = 1;
	while (po)
	{
		if (po%2 == 1) res = (res*base)%MOD;
		base = (base*base)%MOD;
		po /= 2;
	}
	return res;
}

int main()
{
	ios :: sync_with_stdio(false);
	int tc; cin >> tc;
	while (tc--)
	{
		int n,q; cin >> n >> q;
		Graph g(n);
		REP(i,n-1)
		{
			int u,v; cin >> u >> v;
		}
		REP(i,q)
		{
			int u,v, x; cin >> u >> v >> x;
			g.addEdge(u-1,v-1,x);
		}
		int compo = 0;
		vector<bool> visited(n, false);
		vector<int> color(n, -1);
		for(int i = 0; i < n && g.flag == false; i++)
		{
			if (!visited[i])
			{
				compo++;
				color[i] = 0;
				g.dfs(i, -1, visited, color);
			}
		}
		if (g.flag == false)
			cout << modPow(2, compo-1) << "\n";
		else
			cout << "0\n";
	}
}
