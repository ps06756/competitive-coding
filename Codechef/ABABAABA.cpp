#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	ios::sync_with_stdio(false);
	int tc; cin>>tc;
	while (tc--)
	{
		map<char, int> mp;
		string str;
		cin>>str;
		vector<pair<char, int> > vec;
		vec.pb(make_pair(str[0], 1));
		mp[str[0]]++;
		FOR(i, 1, SZ(str))
		{
			mp[str[i]]++;
			auto it = vec.rbegin();
			if (str[i] == it->first)
				it->second++;	
			else
				vec.pb(make_pair(str[i], 1));
		}
		if (mp['A'] == 2)
		{
			cout<<"A\n";
			continue;
		}
		else if (mp['B'] == 2)
		{
			cout<<"B\n";
			continue;
		}
		int pos = -1;
		FOR(i,0, SZ(vec))
		{
			if (vec[i].second == 2)
			{
				pos = i;
				break;
			}
		}
		if (pos != -1)
		{
			string ans = "";
			FOR(i,0,SZ(vec))
			{
				if (i != pos)
				{
					FOR(j, 0, vec[i].second)
						ans = ans + vec[i].first;
				}
				else
					ans = ans + vec[i].first;
			}
			cout<<ans<<"\n";
		}
		else
			cout<<"-1\n";
	}
	return (0);
}
