#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	vector<vector<int> > fact(1000001);
	fact[1].pb(1);
	for(int i = 2; i < SZ(fact); i++)
	{
		if (SZ(fact[i]) == 0)
		{
			for(int j = i; j < SZ(fact); j += i)
				fact[j].pb(i);
		}
	}
	vector<int> last(SZ(fact),-1);
	int tc; scanf("%d", &tc);
	while (tc--)
	{
		fill(all(last), -1);
		int n, prev = 0, ans = 0; scanf("%d", &n);
		REP(i,n)
		{
			int temp; scanf("%d", &temp);
			if (temp != 1)
			{
				int diff = INT_MAX;
				for(int j = 0; j < SZ(fact[temp]); j++)
					diff = min(diff,i - last[fact[temp][j]]);
				prev = min(prev + 1, diff);
				for(int j = 0; j < SZ(fact[temp]); j++)
					last[fact[temp][j]] = i;
			}
			else
				prev = prev + 1;
			ans = max(ans, prev);
		}
		if (ans <= 1)
			printf("-1\n");
		else
			printf("%d\n", ans);
	}
	return (0);
}
