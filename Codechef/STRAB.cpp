#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MAX 21
#define MOD 1000000007
i6 dp[21];
char A[MAX], B[MAX], str[MAX];
int n,m;

int isValid(char* str, int i, int m, int len)
{
	char* str2 = &str[i];
	int length = min(len - i, m);
	if (length == m && strncmp(str2, A, length) >= 0 && strncmp(str2, B, length) <= 0)
		return 0;
	return 1;
}

i6 func(char* str,int i, int len)
{
	if (i == -1)
		return 1;
	else
	{
		i6 ans = 0;
		if (i >= 0)
			ans = (dp[i]*24)%MOD;
		str[i] = 'A';
		ans = (ans + func(str, i-1, len)*isValid(str, i, m, len))%MOD;
		str[i] = 'B';
		ans = (ans + func(str, i-1, len)*isValid(str, i, m, len))%MOD;
		return ans;
	}
}

int main()
{
	int tc; scanf("%d", &tc);
	while (tc--)
	{
		fill(&dp[0], &dp[MAX], 0);
		dp[0] = 1;
		fill(&str[0], &str[MAX], 0);
		scanf("%d%d", &n, &m);
		scanf("%s%s", A,B);
		for(int i = 1;  i < n + 1; i++)
			dp[i] = func(str, i-1, i);
		printf("%lld\n", dp[n]);
	}
	return (0);
}
