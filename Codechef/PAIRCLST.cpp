#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class Graph
{
	public:
		vector<vector<ii> > vec;
		vector<int> special;
		Graph(int n)
		{
			vec = vector<vector<ii> >(n);
			special = vector<int>(n,0);
		}
		void addEdge(int u,int v, int w)
		{
			vec[u].pb(mp(v,w));
			vec[v].pb(mp(u,w));
		}
		void addSpecialNode(int u)
		{
			special[u] = 1;
		}
		int shortest(int source, int maxDistance, vector<bool>& visited)
		{
			vector<int> dist(SZ(vec), numeric_limits<int>::max());
			priority_queue<pair<int,int> , vector<pair<int,int> >, greater<pair<int,int> > > Q;
			Q.push(mp(0,source));
			while (!Q.empty())
			{
				pair<int,int> top = Q.top();
				Q.pop();
				int vertex = top.second;
				if (dist[vertex] >= top.first)
				{
					visited[vertex] = true;
					dist[vertex] = top.first;
					if (special[vertex] == 1 && vertex != source && dist[vertex] <= maxDistance)
						return dist[vertex];
					else if (dist[vertex] >= maxDistance)
						return numeric_limits<int>::max();
					for(pair<int,int> nei : vec[vertex])
					{
						if (dist[nei.first] > dist[vertex] + nei.second)
						{
							dist[nei.first] = dist[vertex] + nei.second;
							Q.push(make_pair(dist[nei.first], nei.first));
						}
					}
				}
			}
			return numeric_limits<int>::max();
		}
};

int main()
{
	ios::sync_with_stdio(false);
	int n,m,k;
	cin>>n>>m>>k;
	Graph g(n);
	REP(i,k)
	{
		int temp; cin>>temp; 
		g.addSpecialNode(temp-1);
	}
	REP(i,m)
	{
		int u,v,w;
		cin>>u>>v>>w;
		g.addEdge(u-1,v-1,w);
	}
	int ans = numeric_limits<int>::max();
	vector<bool> visited(n,false);
	REP(i, SZ(visited))
	{
		if (!visited[i] && g.special[i] == 1)
		{
			ans = min(ans, g.shortest(i, ans, visited));
		}
	}
	cout<<ans<<"\n";
	return (0);
}
