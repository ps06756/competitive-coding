#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

int main()
{
	ios :: sync_with_stdio(false);
	vector<vector<i6> > dp(101, vector<i6>(2601, 0));
	for(int i = 0; i < 26; i++)
		dp[0][i] = 1;
	for(int i = 0; i < SZ(dp)-1; i++)
	{
		for(int j = 0; j < SZ(dp[i]); j++)
		{
			if (dp[i][j])
			{
				for(int k = 0; k < 26; k++)
					dp[i+1][j + k] = (dp[i+1][j+k] + dp[i][j])%MOD;
			}
		}
	}
	int tc; cin >> tc; 
	while (tc--)
	{
		string str; cin >> str;
		int sum = 0;
		REP(i, SZ(str))
			sum += str[i] - 'a';
		cout << (dp[SZ(str)-1][sum] -1 + MOD)%MOD<< "\n";
	}
	return (0);
}
