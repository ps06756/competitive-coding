#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

vector<vector<double> > matrix;
vector<double> dp;
double getProb(int bitmask, int num)
{
	double setBits = 0.0;
	for(int i = 0; i < SZ(matrix); i++)
	{
		if (( bitmask >> i) & 1)
			setBits += 1.0;
	}
	double ans = 0;
	if ((int)setBits%2 == 0)
		setBits = (setBits/2)*(setBits-1);
	else
		setBits = setBits*((setBits - 1)/2);
	for(int i = 0; i < SZ(matrix); i++)
	{
		if (((bitmask >> i)&1) && i != num)
			ans += (matrix[i][num]*(1.0/setBits));
	}
	return ans;
}

int main()
{
	ios :: sync_with_stdio(false);
	int n; cin >> n;
	matrix = vector<vector<double> >(n,vector<double>(n,0.0));
	dp = vector<double> ((1 << 18), 0.0);
	REP(i,n)
	{
		REP(j,n)
			cin >> matrix[i][j]; 
	}
	dp[(1 << n) - 1] = 1.0;
	for(int i = (1 << n) - 1; i >= 0; i--)
	{
		for(int j = 0; j < n; j++)
		{
			if ((i >> j)&1)
				dp[i&~(1 << j)] += (dp[i]*getProb(i, j));
		}
	}
	for(int i = 0; i < n; i++)
		cout << fixed << setprecision(6) << dp[(1 << i)] << " ";
	cout << "\n";
	return (0);
}
