#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_tuple((x), (y))
#define mp3(x,y,z) make_tuple((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

void swapCols(int p1, int p2, vector<vector<int> >& matrix)
{
	for(int i = 0; i < SZ(matrix); i++)
		swap(matrix[i][p1], matrix[i][p2]);
}

void swapRows(int p1, int p2, vector<vector<int> >& matrix)
{
	for(int i = 0; i < SZ(matrix[0]); i++)
		swap(matrix[p1][i], matrix[p2][i]);
}

int main()
{
	ios :: sync_with_stdio(false);
	cin.tie(NULL);
	int n; cin >> n;
	vector<vector<int> > matrix(n, vector<int>(n,0));
	vector<tuple<int,int,int> > operations;
	vector<int> cols(n), rows(n);
	REP(i,n-1)
	{
		int u,v; cin >> u >> v;
		u--; v--;
		matrix[u][v] = 1;
		rows[u]++; cols[v]++;
	}
	int dim = n-1;
	while (dim != 0)
	{
		for(int i = 0; i <= dim; i++)
		{
			if (cols[i] == 0)
			{
				swap(cols[i], cols[dim]);
				swapCols(i, dim, matrix);
				if (i + 1 != dim + 1)
					operations.push_back(make_tuple(2, i + 1, dim + 1));
				break;
			}
		}
		for(int i = 0; i <= dim; i++)
		{
			if (rows[i] != 0)
			{
				swap(rows[i], rows[dim]);
				swapRows(i, dim, matrix);
				if (i + 1 != dim + 1)
				operations.push_back(make_tuple(1, i + 1, dim + 1));
				break;
			}
		}
		for(int i = 0; i <= dim; i++)
		{
			if (matrix[dim][i] == 1)
				cols[i]--;
			if (matrix[i][dim] == 1)
				rows[i]--;
		}
		dim--;
	}
	cout << SZ(operations) << "\n";
	for(tuple<int,int,int> tup : operations)
		cout << get<0>(tup) << " " << get<1>(tup) << " " << get<2>(tup) << "\n";
}
