#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	ios::sync_with_stdio(false);
	int n, d, h;
	cin>>n>>d>>h;
	if (d < h || d > 2*h || h >= n || d >= n || ( h <= 0 || d <= 0))
	{
		cout<<"-1\n";
		return (0);
	}
	if (d == h && d != n-1 && d == 1)
	{
		cout<<"-1\n";
		return (0);
	}
	else if (d != h)
	{
		int prev = 1, cnt = 0;
		for(int i = 2; i <= h+1; i++)
		{
			cout<<prev<<" "<<i<<"\n";
			prev = i;
			cnt++;
		}
		int prev2 = 1;
		prev++;
		for(int i = 0; i < d-h; i++)
		{
			cout<<prev2<<" "<<prev<<"\n";
			prev2 = prev;
			prev++;
			cnt++;
		}
		for(int i = 0; i < n - 1 - cnt; i++)
		{
			cout<<1<<" "<<prev<<"\n";
			prev++;
		}
	}
	else
	{
		int prev = 1, cnt = 0;	
		for(int i = 2; i <= h + 1; i++)
		{
			cout<<prev<<" "<<i<<"\n";
			prev = i;
			cnt++;
		}
		prev++;
		for(int i = 0; i < n - 1 - cnt; i++)
		{
			cout<<2<<" "<<prev<<"\n";
			prev++;
		}
	}
	return (0);
}
