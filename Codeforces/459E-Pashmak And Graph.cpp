#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	ios :: sync_with_stdio(false);
	map<int, vector<pair<int,int> > > edges;
	int n,m; cin >> n >> m;
	REP(i,m)
	{
		int u,v,w; cin >> u >> v >> w;
		u--; v--;
		edges[w].push_back(make_pair(u,v));
	}
	vector<int> dp(n,0), dp2(n,0);
	for(auto it = edges.begin(); it != edges.end(); it++)
	{
		for(auto it2 = edges[it->first].begin(); it2 != edges[it->first].end(); it2++)
		{
			dp2[it2->second] = max(dp2[it2->second], dp[it2->first] + 1);
		}
		for(auto it2 = edges[it->first].begin(); it2 != edges[it->first].end(); it2++)
			dp[it2->second] = dp2[it2->second];
	}
	cout << *max_element(all(dp)) << "\n";
	return (0);
}
