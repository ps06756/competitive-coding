#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	ios :: sync_with_stdio(false);
	int n,m; cin >> n >> m;
	vector<vector<int> > rows(n), cols(n), primDiag(2*n), seconDiag(2*n);
	vector<pair<int,int> > vec;
	REP(i,m)
	{
		int u, v; cin >> u >> v;
		u--; v--;
		vec.push_back(make_pair(u,v));
		rows[u].pb(v);
		cols[v].pb(u);
		primDiag[u + v].pb(v);
		seconDiag[n - u + v].pb(u);
	}
	for(int i = 0; i < n; i++)
	{
		sort(all(rows[i]));
		sort(all(cols[i]));
		sort(all(primDiag[i]));
		sort(all(seconDiag[i]));
	}
	for(int i = n; i < 2*n; i++)
	{
		sort(all(primDiag[i]));
		sort(all(seconDiag[i]));
	}
	vector<int> finalAns(9,0);
	for(pair<int,int> p : vec)
	{
		int i = p.first, j = p.second;
		int cnt = 0;
		auto it = lower_bound(all(rows[i]), j);
		if (it != rows[i].end() && ((it == rows[i].begin() && SZ(rows[i]) > 1) ||  (it  == --rows[i].end() && SZ(rows[i]) > 1)))
			cnt++;
		else if (it != rows[i].end() && SZ(rows[i]) > 2)
			cnt += 2;
		it = lower_bound(all(cols[j]),i);
		if (it != cols[j].end() && ((it == cols[j].begin() && SZ(cols[j]) > 1) || (it == --cols[j].end() && SZ(cols[j]) > 1)))
			cnt++;
		else if (it != cols[j].end() && SZ(cols[j]) > 2)
			cnt += 2;
		it = lower_bound(all(primDiag[i + j]), j);
		if (it != primDiag[i+j].end() && ((it == primDiag[i + j].begin() && SZ(primDiag[i + j]) > 1) || (it  == --primDiag[i + j].end() && SZ(primDiag[i + j]) > 1)))
			cnt++;
		else if (it != primDiag[i + j].end() && SZ(primDiag[i + j]) > 2)
			cnt += 2;
		it = lower_bound(all(seconDiag[n - i + j]), i);
		if (it != seconDiag[n - i + j].end() && ((it == seconDiag[n - i + j].begin() && SZ(seconDiag[n - i + j]) > 1) || (it  == --seconDiag[n - i + j].end() && SZ(seconDiag[n - i + j]) > 1)))
			cnt++;
		else if (it != seconDiag[n - i + j].end() && SZ(seconDiag[n - i + j]) > 2)
			cnt += 2;
		finalAns[cnt]++;
	}
	for(int i = 0; i < 9; i++)
		cout << finalAns[i] << " ";
	cout << "\n";
	return (0);
}
