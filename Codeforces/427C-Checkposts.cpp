#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
#define MOD 1000000007 
typedef vector<int> vi;
typedef map<int,int> mii;
typedef set<int> si;
typedef pair<int,int> pii;
typedef vector<pair<int,int> > vpii;

class Graph
{
	public:
		vector<vector<int> > vec;
	Graph(int n)
	{
		vec = vector<vector<int> > (n);
	}
	void addEdge(int u,int v)
	{
		vec[u].pb(v);
	}
	void fDfs(int source, vector<bool>& visited, stack<int>& st)
	{
		if (!visited[source])
		{
			visited[source] = true;
			foreach(vec[source], it)
				fDfs(*it, visited, st);
			st.push(source);
		}
	}
	void sDfs(int source, vector<bool>& visited, vector<int>& ve, vector<int>& cost)
	{
		if (!visited[source])
		{
			ve.push_back(cost[source]);
			visited[source] = true;
			foreach(vec[source], it)
			{
				sDfs(*it, visited, ve, cost);
			}
		}
	}
};

int main()
{
	ios::sync_with_stdio(false);
	int n; cin>>n;
	Graph g(n), g2(n);
	vector<int> cost(n);
	REP(i,n)
		cin>>cost[i];
	int m;
	cin>>m;
	REP(i,m)
	{
		int u, v;
		cin>>u>>v;
		u--; v--;
		g.addEdge(u,v);
		g2.addEdge(v,u);
	}
	vector<bool> visited(n,false);
	stack<int> st;
	REP(i,n)
	{
		if (!visited[i])
			g.fDfs(i, visited, st);
	}
	vector<vector<int> > groups;
	fill(all(visited), false);
	while(!st.empty())
	{
		vector<int> vec;
		int t = st.top();
		st.pop();
		if (!visited[t])
		{
			g2.sDfs(t, visited, vec, cost);
			groups.pb(vec);
		}
	}
	int64_t ans = 0;
	int64_t an = 1;
	REP(i, SZ(groups))
	{
		if (SZ(groups[i]) > 0)
		{
			int temp = *min_element(all(groups[i]));
			ans += *min_element(all(groups[i]));
			an *= count(all(groups[i]), temp);
			an %= MOD;
		}
	}
	cout<<ans<<" "<<an<<"\n";
}
