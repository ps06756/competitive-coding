#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
vector<bool> visi;

class Graph
{
	public:
		vector<vector<int> > vec;
		Graph(int n)
		{
			vec = vector<vector<int> > (n);
		}
		void addEdge(int u, int v)
		{
			vec[u].pb(v);
			vec[v].pb(u);
		}
		void dfs(int source, vector<bool>& visited, int cnt, int& ans, int& node)
		{
			if (!visited[source])
			{
				if (cnt >= ans)
				{
					ans = cnt; 
					node = source;
				}
				visited[source] = true;
				foreach(vec[source], it)
				{
					if (!visited[*it])
						dfs(*it, visited, cnt + 1, ans, node);
				}
			}
		}
		int getDiameter(int source, vector<bool>& visited)
		{
			int ans = 0, node = 0, ans2 = 0, node2 = 0;
			dfs(source, visited, 0, ans, node);
			dfs(node, visi, 0, ans2, node2);
			return ans2;
		}
};
class DSU
{
	public:
		vector<int> parent, diameter;
		DSU(int n)
		{
			parent = vector<int>(n);
			diameter = vector<int> (n,0);
			for(int i = 0 ; i < n; i++)
				parent[i] = i;
		}
		int findSet(int source)
		{
			if (source != parent[source]) parent[source] = findSet(parent[source]);
			return parent[source];
		}
		int getDiameter(int source)
		{
			int p = findSet(source);
			return diameter[p];
		}
		void mergeSet(int x, int y)
		{
			int px = findSet(x), py = findSet(y);
			if (px == py) return;
			parent[py] = px;
			diameter[px] = max(diameter[py], max(diameter[px], (diameter[py] + 1)/2 + (diameter[px] + 1)/2 + 1));
		}
};

int main()
{
	int n, m, q;
	scanf("%d%d%d", &n, &m, &q);
	Graph g(n);
	vector<bool> visited(n,false);
	visi = vector<bool>(n,false);
	DSU dsu(n);
	REP(i,m)
	{
		int u,v; scanf("%d%d", &u, &v); u--; v--;
		dsu.mergeSet(u,v);
		g.addEdge(u,v);

	}
	for(int i = 0; i < n; i++)
	{
		if (!visited[i])
			dsu.diameter[dsu.findSet(i)] = g.getDiameter(i, visited);
	}
	REP(i,q)
	{
		int u,v,z;
		scanf("%d", &u);
		if (u == 1)
		{
			scanf("%d", &v);
			printf("%d\n", dsu.getDiameter(v - 1));
		}
		else 
		{
			scanf("%d%d", &v, &z);
			dsu.mergeSet(v - 1, z - 1);
		}
	}
	return (0);
}

