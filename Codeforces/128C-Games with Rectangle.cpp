#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MAX 2002
#define MOD 1000000007

vector<vector<i6> > dp(MAX, vector<i6>(MAX,0));

int main()
{
	ios :: sync_with_stdio(false);
	i6 n,m,k;
	dp[0][0] = 1;
	for(int i = 1; i < SZ(dp); i++)
	{
		dp[i][0] = 1;
		for(int j = 1; j < SZ(dp[i]); j++)
			dp[i][j] = (dp[i-1][j] + dp[i-1][j-1])%MOD;
	}
	cin >> n >> m >> k;
	cout << (dp[n-1][2*k]*dp[m-1][2*k])%MOD << "\n";
	return (0);
}
