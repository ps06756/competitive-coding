#include <bits/stdc++.h>
using namespace std;

int main()
{
	int n; scanf("%d", &n);
	vector<pair<int, int> > vec(n);
	vector<int> dp(n + 1);;
	for (int i = 0; i < n; i++)
		scanf("%d%d", &vec[i].first, &vec[i].second);
	sort(vec.begin(), vec.end());

	dp[0] = 0; dp[1] = 0; 
	for (int i = 1; i < n; i++)
	{
		vector<pair<int, int> >::iterator it = lower_bound(vec.begin(), vec.end(), make_pair(vec[i].first - vec[i].second, 0));

		if (it == vec.begin() + i) /* nothing to destroy */
		{
			dp[i+1] = dp[i];
		}
		else if (it == vec.begin())
		{
			dp[i+1] = i;
		}
		else 
		{
			dp[i+1] = vec.begin() + i - it; it--;
			dp[i+1] += dp[1 + it - vec.begin()];
		}
	}
	int minAns = INT_MAX;
	for (int i = 0; i < n+1; i++)
	{
		minAns = min(minAns, i + dp[n - i]);
	}
	printf("%d\n", minAns);
}
