#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
vector<vector<i6> > tree;
int wi = 0;

void mergeSort(vector<i6>& arr, int start, int end, int index)
{
	if (end - start == 1)
	{
		if (arr[start] > arr[end])
		{
			tree[wi][index] = 1;
			swap(arr[start], arr[end]);
		}
		else
			tree[wi][index] = 0;
	}
	else if (end - start > 1)
	{
		int mid = (start + end)/2;
		mergeSort(arr, start, mid, 2*index + 1);
		mergeSort(arr, mid + 1, end, 2*index + 2);
		vector<int> temp;
		i6 i = 0, j = 0, cnt = 0;
		while (i < mid - start + 1 && j < end - mid )
		{
			if (arr[start + i] <= arr[mid + 1 + j])
			{
				temp.pb(arr[start + i]);
				i++;
				cnt += j;
			}
			else
			{
				cnt++;
				temp.pb(arr[mid + 1 + j]);
				j++;
			}
		}
		for(; i < mid - start + 1; i++)
		{
			temp.pb(arr[start + i]);
			cnt += j;
		}
		cnt -= j;
		for(; j < end - mid; j++)
			temp.pb(arr[mid + 1 + j]);
		for(int i = 0; i < SZ(temp); i++)
			arr[start + i] = temp[i];
		tree[wi][index] = cnt;
	}
}

int main()
{
	int n; scanf("%d", &n);
	vector<i6> vec((1 << n), 0), temp((1 << n),0), cu1(n+1,0), cu2(n+1,0);
	tree = vector<vector<i6> > (2, vector<i6> (1 << n));
	vector<int> current(n+1,0);
	REP(i,(1 << n))
	{
		scanf("%d", &vec[i]);
		temp[i] = vec[i];
	}
	mergeSort(vec, 0, SZ(vec) - 1, 0);
	reverse(all(temp));
	wi = 1;
	mergeSort(temp, 0, SZ(temp) - 1, 0);
	for(int i = 0; i < SZ(vec); i++)
	{	
		int level = log2(i + 1);
		cu1[n - level] += tree[0][i];
		cu2[n - level] += tree[1][i];
	}
	int m; scanf("%d", &m);
	while (m--)
	{
		int q; scanf("%d", &q);
		for(int i = q; i >= 1; i--)
			current[i] ^= 1;
		i6 ans = 0;
		for(int i = 0; i <= n; i++)
		{
			if (current[i] == 0)
				ans += cu1[i];
			else
				ans += cu2[i];
		}
		cout << ans << "\n";
	}
	return (0);
}

