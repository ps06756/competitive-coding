#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	ios :: sync_with_stdio(false);
	string str; cin >> str;
	stack<char> st;
	stack<int> pos;
	vector<pair<char,char> > vec = { {'[', ']'}, {'(', ')'} };
	vector<tuple<int,int,int> > sol;
	vector<pair<int,int> > ans;
	if (str[0] == '(' || str[0] == '[')
	{
		st.push(str[0]);
		pos.push(0);
	}
	for(int i = 1; i < SZ(str); i++)
	{
		if (str[i] == '(' || str[i] == '[')
		{
			st.push(str[i]);
			pos.push(i);
		}
		else
		{
			if (st.size() == 0) continue;
			pair<char,char> p = make_pair(st.top(), str[i]);
			if(find(all(vec), p) != vec.end())
			{
				pair<int,int> temp = make_pair(pos.top(), i);
				if (!ans.empty())
				{
					int n = SZ(ans);
					if (ans[n-1].second + 1 == pos.top())
						ans[n-1].second = i;
					else
						ans.push_back(temp);
				}
				else
					ans.push_back(temp);
				st.pop();
				pos.pop();
			}
			else
			{
				while (!st.empty())
				{
					st.pop();
					pos.pop();
				}
			}
		}
	}
	pair<int,int> far = {0, 0} ;
	sort(all(ans));
	for(int i = 1; i < SZ(ans); i++)
	{
		if (ans[i].first >= ans[i-1].first && ans[i].second <= ans[i-1].second)
			ans[i] = ans[i-1];
		else if (ans[i].first == ans[i-1].second + 1)
			ans[i].first = ans[i-1].first;
	}
	for(int i = 0; i < SZ(ans); i++)
	{
		if (ans[i].first >= far.first && ans[i].second <= far.second) continue;
		else if (ans[i].second - ans[i].first >= far.second - far.first)
			far = ans[i];
		int cnt = 0;
		for(int start = ans[i].first; start <= ans[i].second; start++)
		{
			if (str[start] == '[')
				cnt++;
		}
		sol.push_back(make_tuple(cnt, ans[i].first, ans[i].second));
	}
	if (SZ(sol) > 0)
	{
		auto it = max_element(all(sol));
		cout << get<0>(*it) << "\n";
		cout << str.substr(get<1>(*it), get<2>(*it) - get<1>(*it) + 1) << "\n";
	}
	else
		cout << "0\n";
}
