#include <bits/stdc++.h>
using namespace std;
#define MAX 200001
char B[MAX];
char A[MAX];

int main()
{
	scanf("%s", A); scanf("%s", B);
	long long  len1 = strlen(A), len2 = strlen(B);
	vector<long long> dp1(len2+1), dp2(len2+1);

	dp1[0] = 0; dp2[0] = 0;
	for (long long  i = 1; i < len2+1; i++)
	{
		if (B[i-1] != '0')
			dp1[i] = dp1[i-1] + 1ll;
		else 
			dp1[i] = dp1[i-1];
		if (B[i-1] != '1')
			dp2[i] = dp2[i-1] + 1ll;
		else
			dp2[i] = dp2[i-1];
	}

	long long cnt = 0;
	long long diff = len2 - len1;
	for (int i = 0; i < len1; i++)
	{
		if ( A[i] == '0')
		{
			cnt += dp1[diff + i + 1] - dp1[i];
		}
		else if (A[i] == '1')
		{
			cnt += dp2[diff + i + 1] - dp2[i];
		}
	}
	printf("%lld\n", cnt);
}

