#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class Graph
{
	public:
		vector<vi> vec;
		vi parent, level,l1;
		vector<vi> pan;
		vector<bool> visited;
		void addEdge(int u, int v)
		{
			vec[u].pb(v);
			vec[v].pb(u);
		}
		Graph(int n)
		{
			vec = vector<vi>(n);
			parent = vi(n,-1);
			i3 size = ceil(log2(n));
			pan = vector<vi>(n, vi(size+1,-1));
			visited = vector<bool>(n,false);
			l1 = vi(n,0);
			level = vi(n,0);
		}
		void dfs(int source, int par, int lv)
		{
			if (!visited[source])
			{
				level[source] = lv;
				visited[source] = true;
				parent[source] = par;	
				foreach(vec[source], it)
				{
					if (!visited[*it])
						dfs(*it, source, lv+1);
				}
			}
		}
		int dfs(int source, vector<bool>& vis, map<pair<int,int>, int>& mp, int parent)
		{
			if (!vis[source])
			{
				vis[source] = true;
				i6 sum = 0;
				foreach(vec[source], it)
				{
					if (!vis[*it] && *it != parent)
					{
						mp[make_pair(source, *it)] = dfs(*it, vis, mp, source);
						sum += mp[make_pair(source,*it)];
					}
				}
				sum += l1[source];
				return sum;
			}
			else
				return (0);
		}
		void preProcess()
		{
			fill(all(visited), false);
			dfs(0,-1,0);
			for(int i = 0; i < SZ(vec); i++)
				pan[i][0] = parent[i];
			for(int j = 1; (1 << j) < SZ(vec); j++)
			{
				for(int i = 0; i < SZ(vec); i++)
				{
					if (pan[i][j-1] != -1)
						pan[i][j] = pan[pan[i][j-1]][j-1];
				}
			}
		}
		int lca(int u,int v)
		{
			if (u == v)
				return u;
			else
			{
				if (level[u] < level[v])
					swap(u,v);
				int k = ceil(log2(level[u]));
				for(int i = k; i >= 0; i--)
				{
					if (level[u] - (1 << i) >= level[v])
						u = pan[u][i];
				}
				if (u == v)
					return (u == -1 ? 0 : u);
				else
				{
					for(int i = k; i >= 0; i--)
					{
						if (pan[u][i] != -1 && pan[u][i] != pan[v][i])
						{
							u = pan[u][i];
							v = pan[v][i];
						}
					}
					return (parent[u] == -1 ? 0 : parent[u]);
				}
			}
		}
};


int main()
{
	ios :: sync_with_stdio(false);
	int n; cin >> n;
	Graph g(n);
	vector<pair<int,int> > vec;
	REP(i,n-1)
	{
		int u,v; cin >> u >> v; u--; v--;
		vec.push_back(make_pair(u,v));
		g.addEdge(u,v);
	}
	g.preProcess();
	int k; cin >> k;
	REP(i,k)
	{
		int u,v; cin >> u >> v;
		u--; v--;
		int lc = g.lca(u,v);
		g.l1[u]++;
		g.l1[v]++;
		g.l1[lc] -= 2;
	}
	vector<bool> visited(n, false);
	map<pair<int,int>,int > mp;
	g.dfs(0, visited,mp,0);
	for(int i = 0; i < SZ(vec); i++)
	{
		pair<int,int> rev = make_pair(vec[i].second, vec[i].first);
		if (mp.count(rev) == 1)
			cout << mp[rev] << " ";
		else
			cout << mp[vec[i]] << " ";
	}
	cout << "\n";
	return (0);
}
