#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	int n,q; scanf("%d%d", &n, &q);
	vector<int> mp(n,0), ans(n,0);
	mp[0] = 0; mp[1] = 1;
	REP(i,q)
	{
		int type; int q;
		scanf("%d", &type);
		if (type == 2)
		{
			if (mp[0] %2 == 0)
				mp[0] = (mp[0] + 1);
			else
				mp[0] = (mp[0] - 1);
			if (mp[0] < 0 || mp[0] >= n)
				mp[0] = (mp[0] + n)%n;
			if (mp[1]%2 == 0)
				mp[1] = (mp[1] + 1 + n);
			else 
				mp[1] = (mp[1] - 1 + n);
			if (mp[1] < 0 || mp[1] >= n)
				mp[1] = (mp[1] + n)%n;
		}
		else
		{
			scanf("%d", &q);
			mp[0] = (mp[0] + q);
			if (mp[0] < 0 || mp[0] >= n)
				mp[0] = (mp[0] + n)%n;
			mp[1] = (mp[1] + q);
			if (mp[1] < 0 || mp[1] >= n)
				mp[1] = (mp[1] + n)%n;
		}
	}
	for(int i = 2; i < n; i += 2)
		mp[i] = (mp[i-2] + 2)%n;
	for(int i = 3; i < n; i += 2)
		mp[i] = (mp[i-2] + 2)%n;
	for(int i = 0; i < (int)mp.size(); i++)
		ans[mp[i]] = i + 1;
	for(int i = 0; i < SZ(ans); i++)
		printf("%d ", ans[i]);
	cout << "\n";
	return (0);
}
