#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class Graph
{
	public:
		vector<vector<int> > vec;
		Graph(int n)
		{
			vec = vector<vector<int> >(n);
		}
		void addEdge(int u, int v)
		{
			vec[u].pb(v);
			vec[v].pb(u);
		}
		int bfs(int source, vector<bool>& visited)
		{
			int ans = 0;
			queue<int> Q;
			queue<int> height;
			Q.push(source);
			height.push(1);
			while (!Q.empty())
			{
				int top = Q.front();
				int hei = height.front();
				ans = max(ans, hei);
				Q.pop();
				height.pop();
				if (!visited[top])
				{
					visited[top] = true;
					foreach(vec[top], it)
					{
						if (!visited[*it])
						{
							Q.push(*it);
							height.push(hei + 1);
						}
					}
				}
			}
			return ans;
		}
};

int main()
{
	ios :: sync_with_stdio(false);
	int n; cin >> n;
	Graph g(n);
	vector<int> v;
	REP(i,n)
	{
		int temp; cin >> temp;	
		if (temp != -1)
			g.addEdge(temp-1, i);
		else
			v.pb(i);
	}
	vector<bool> visited(SZ(g.vec), false);
	int height = 0;
	for(int i :  v)
		height = max(g.bfs(i, visited), height);
	cout << height << "\n";
}
