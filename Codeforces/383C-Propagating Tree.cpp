#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class Graph
{
	public:
		vector<vector<int> > vec;
		vector<int> level, start, end, mp;
		Graph(int n)
		{
			vec = vector<vector<int> > (n);
			level = vector<int>(n,-1);
			start = vector<int>(n,-1);
			mp = vector<int>(n,-1);
			end = vector<int> (n,-1);
		}
		void addEdge(int u, int v)
		{
			vec[u].pb(v);
			vec[v].pb(u);
		}
		void dfs(int source, int lv, int& current)
		{
			if (level[source] == -1)
			{
				mp[source] = current;
				start[source] = current;
				level[source] = lv + 1;
				foreach(vec[source], it)
				{
					if (level[*it] == -1)
					{
						current++;
						dfs(*it, level[source], current);
					}
				}
				end[source] = current;
			}
		}
};

template<typename T, typename R>
class BIT
{
	public:
		vector<T> tree;
		BIT(int n)
		{
			for (int i = 0; i < n; i++)
				tree.push_back(0);
		}
		R getSum(int idx)
		{
			R cnt = 0;
			while (idx > 0)
			{
				cnt += tree[idx];
				idx -= (idx & -idx);
			}
			return cnt;
		}
		void update(int idx, T value)
		{
			while (idx <= (int)tree.size())
			{
				tree[idx] += value;
				idx += (idx & -idx);
			}
		}
};





int main()
{
	ios :: sync_with_stdio(false);
	cin.tie(NULL);
	int n,m; cin >> n >> m;	
	vector<int> initial(n+1,0);
	BIT<i6,i6> bit(n+1);
	Graph g(n);
	REP(i,n)
	{
		int temp; cin >> temp;
		initial[i+1] = temp;
	}
	REP(i,n-1)
	{
		int u,v; cin >> u >> v;
		g.addEdge(u-1,v-1);
	}

	int current = 1;
	g.dfs(0, -1,current);
	REP(i,m)
	{
		int q, v1, v2;
		cin >> q >> v1;
		if (q == 1)
		{
			cin >> v2;
			if (g.level[v1-1]%2 != 0)
				v2 = -v2;
			bit.update(g.start[v1-1], v2);
			bit.update(g.end[v1-1] + 1, -v2);
		}
		else
		{
			v1--;
			if (g.level[v1]&1)
				cout << initial[v1 + 1] - bit.getSum(g.mp[v1]) << "\n";
			else
				cout << initial[v1 + 1] + bit.getSum(g.mp[v1]) << "\n";
		}
	}
	return (0);
}
