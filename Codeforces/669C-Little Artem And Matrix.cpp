#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

void shiftRow(vector<vector<pair<int,int> > > & matrix, int row)
{
	int n = SZ(matrix[row]);
	vector<pair<int,int> > arr(SZ(matrix[row]));
	for(int i = 0; i < n; i++)
		arr[i] = matrix[row][(i+1)%n];
	for(int i = 0; i < n; i++)
		matrix[row][i] = arr[i];
}

void shiftColumn(vector<vector<pair<int,int> > >& matrix, int col)
{
	int n = SZ(matrix);
	vector<pair<int,int> > arr(n);
	for(int j = 0; j < n; j++)
		arr[j] = matrix[(j+1)%n][col];
	for(int j = 0; j < n; j++)
		matrix[j][col] = arr[j];
}

int main()
{
	ios :: sync_with_stdio(false);
	int n,m,q; cin >> n >> m >> q;
	vector<vector<pair<int,int> > > matrix(n, vector<pair<int,int> >(m));
	vector<vector<int> > orig(n, vector<int>(m));
	for(int i = 0; i < n ;i++)
	{	
		for(int j = 0; j < m; j++)
			matrix[i][j] = make_pair(i,j);
	}
	while (q--)
	{
		int type, a, b, c;
		cin >> type;
		if (type == 1)
		{
			cin >> a;
			a--;
			shiftRow(matrix, a);
		}
		else if (type == 2)
		{
			cin >> b;
			b--;
			shiftColumn(matrix, b);
		}
		else
		{
			cin >> a >> b >> c;
			a--; b--;
			orig[matrix[a][b].first][matrix[a][b].second] = c;
		}
	}
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
			cout << orig[i][j] << " ";
		cout << "\n";
	}
	return (0);
}

