#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
string answer;

int recurse(string& str, int pos)
{
	if (pos >= SZ(str))
	{
		cout << "Error occurred\n";
		exit(0);
	}
	if (str[pos] == 'B')
	{
		answer += "B";
		return (pos);
	}
	else if (str[pos] == 'A')
	{
		answer += "A<";
		int r1 = recurse(str, pos + 2);
		answer += ",";
		int r2 = recurse(str, r1 + 2);
		answer += ">";
		return r2;
	}
}

int main()
{
	string str;		
	int n; cin >> n;
	bool flag = true;
	int cnt1 = 0, cnt2 = 0;
	cin.ignore(1000, '\n');
	string temp, line;
	vector<string> vec;
	getline(cin, line);
	istringstream ss(line);
	while(ss >> temp)
		vec.push_back(temp);
	REP(i,SZ(vec))
	{
		temp = vec[i];
		if (temp == "pair")
		{
			str += "A ";
			cnt1++;
		}
		else
		{
			str += "B ";
			cnt2++;
		}
		if (cnt1 - cnt2 < 0 && i != SZ(vec)-1)
			flag = false;
		else if (cnt1 - cnt2 != -1 && i == SZ(vec)-1)
			flag = false;
	}
	recurse(str, 0);
	string r1 = answer;
	if (flag == false)
		cout << "Error occurred\n";
	else
	{
		for(int i = 0; i < SZ(r1); i++)
		{
			if (r1[i] == 'A') cout << "pair";
			else if (r1[i] == 'B') cout << "int";
			else cout << r1[i];
		}
		cout << "\n";
	}
	return (0);
}
