#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

i6 ans = 0;
class Node;
class Node
{
	public:	
		i6 num;
		Node* left;
		Node* right;
		Node(i6 n)
		{
			left = right = NULL;
			num = n;
		}
		Node()
		{
			left = right = NULL;
		}
};
Node* root = NULL;

void addWord(Node* node, i6 num, int cnt)
{
	if (cnt < 0)
		return;
	if (!((num >> cnt)&1))
	{
		if (node->left == NULL)
			node->left = new Node(num);
		addWord(node->left, num, cnt - 1);
	}
	else
	{
		if (node->right == NULL)
			node->right = new Node(num);
		addWord(node->right, num, cnt - 1);
	}
}

void getBest(Node* node, i6 cnt, i6 num)
{
	if (cnt < 0)
		return;
	if ((num >> cnt)&1)
	{
		if (node->left != NULL)
		{
			ans = ans | (1LL << cnt);
			getBest(node->left, cnt - 1, num);
		}
		else
			getBest(node->right, cnt - 1, num);
	}
	else
	{
		if (node->right != NULL)
		{
			ans = ans | (1LL << cnt);
			getBest(node->right, cnt - 1, num);
		}
		else
			getBest(node->left, cnt - 1, num);
	}
}

int main()
{
	ios :: sync_with_stdio(false);
	int bits = sizeof(i6)*8; bits--;
	i6 n; cin >> n;
	vector<i6> vec(n);
	i6 xorVal = 0, xorVal2 = 0, maxVal = 0;
	REP(i,n)
	{
		cin >> vec[i];
		xorVal2 ^= vec[i];
	}
	root = new Node;
	addWord(root, 0,bits);
	maxVal = xorVal2;
	for(i6 i = n-1; i >= 0; i--)
	{
		xorVal2 ^= vec[i];
		ans = 0;
		xorVal ^= vec[i];
		addWord(root, xorVal, bits);
		getBest(root, bits, xorVal2);
		maxVal = max(maxVal,ans);
	}
	cout << maxVal << "\n";
	return (0);
}
