#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

struct edge
{
	int v, c, o, i;
	edge(int ver, int co, int ori, int id)
	{
		v = ver; c = co; o = ori; i = id;
	}
};

int main()
{
	ios :: sync_with_stdio(false);
	int n,m; cin >> n >> m;
	vector<vector<edge> > edges(n);
	vector<int> direction(m,-1), f(n,0);
	REP(i,m)
	{
		int u,v,c; cin >> u >> v >> c;
		u--; v--;
		edges[u].push_back(edge(v,c,0,i));
		edges[v].push_back(edge(u,c,1,i));
		f[u] += c;
		f[v] += c;
	}
	for(int i = 0; i < SZ(f); i++)
		f[i] /= 2;
	queue<int> Q;
	Q.push(0);
	while (!Q.empty())
	{
		int u = Q.front();
		Q.pop();
		for(int i = 0; i < (int)edges[u].size(); i++)
		{
			if (direction[edges[u][i].i] == -1)
			{
				int vertex = edges[u][i].v;
				direction[edges[u][i].i] = edges[u][i].o;
				f[vertex] -= edges[u][i].c;
				if (vertex != n-1 && f[vertex] == 0)
					Q.push(vertex);
			}
		}
	}
	for(auto num : direction)
		cout << num << "\n";
	return (0);
}
