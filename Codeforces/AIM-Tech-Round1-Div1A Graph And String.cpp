#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class Graph
{
	public:
		vector<vector<int> > vec;
		vector<vector<int> > adj;
		map<int,char> mp;
		Graph(int n)
		{
			vec = vector<vector<int> > (n);
			adj = vector<vector<int> > (n, vector<int>(n,0));
			mp = map<int,char>();
		}
		void addEdge(int u, int v)
		{
			vec[u].pb(v);
			adj[u][v] = 1;
			adj[v][u] = 1;
			vec[v].pb(u);
		}
		bool markAndCheck()
		{
			int source = -1, n = SZ(vec);
			for(int i = 0; i < SZ(vec); i++)
			{
				if (SZ(vec[i]) == SZ(vec)-1)
					mp[i] = 'b';
				else if (source == -1)
					source = i;
			}
			if (source < 0) return true;
			mp[source] = 'a';
			for(int i = 0; i < n; i++)
			{
				if (adj[source][i] > 0 && mp.count(i) == 0)
					mp[i] = 'a';
			}
			for(int i = 0; i < n; i++)
			{
				if (mp.count(i) == 0)
					mp[i] = 'c';
			}
			for(int i = 0; i < n; i++)
			{
				for(int j = 0; j < n; j++)
				{
					if (i == j) continue;
					if (adj[i][j] > 0 && abs(mp[i] - mp[j]) >= 2)
						return false;
					if (adj[i][j] == 0 && abs(mp[i] - mp[j]) < 2)
						return false;
				}
			}
			return true;
		}
};

int main()
{
	int n, m; cin >> n >> m;
	Graph g(n);
	REP(i,m)
	{
		int u,v; cin >> u >> v;
		g.addEdge(u-1,v-1);
	}
	if (g.markAndCheck())
	{
		cout << "Yes\n";
		for(pair<const int,char>& p : g.mp)
			cout << p.second;
		cout << "\n";
	}
	else
		cout << "No\n";
	return (0);
}
