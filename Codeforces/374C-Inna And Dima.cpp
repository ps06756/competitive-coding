#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
string ss = "IMAD";
vector<string> vec;
vector<vector<i6> > dp;

i6 dfs(int i,int j, int index)
{

	if (dp[i][j] != 0) return dp[i][j];
	i6 temp = 0;
	dp[i][j] = INT_MAX; index = index%4;
	if (i > 0 && vec[i-1][j] == ss[index]) temp = max(temp, dfs(i - 1, j, index + 1));
	if (i < SZ(vec) - 1 && vec[i+1][j] == ss[index]) temp = max(temp, dfs(i + 1, j , index + 1));
	if (j > 0 && vec[i][j-1] == ss[index]) temp = max(temp, dfs(i, j - 1, index + 1));
	if (j < SZ(vec[0]) - 1 && vec[i][j+1] == ss[index]) temp = max(temp, dfs(i, j + 1, index + 1));
	dp[i][j] = min((i6)INT_MAX, temp + 1);
	return dp[i][j];
}

int main()
{
	ios :: sync_with_stdio(false);
	int n,m; cin >> n >> m;
	vec = vector<string>(n);
	REP(i,n)
		cin >> vec[i];	
	dp = vector<vector<i6> >(n, vector<i6>(m,0));
	i6 ma = 0;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			if (vec[i][j] == 'D') ma = max(ma, dfs(i,j,0));
		}
	}
	if (ma < 4)
		cout << "Poor Dima!\n";
	else if (ma >= INT_MAX)
		cout << "Poor Inna!\n";
	else 
		cout << ma/4 << "\n";
	return (0);
}
