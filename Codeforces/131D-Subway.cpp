#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class Graph
{
	public:
		vector<vector<int> > vec;
		bool cycle;
		Graph(int n)
		{
			vec = vector<vector<int> > (n);
			cycle = false;
		}
		void addEdge(int u, int v)
		{
			vec[u].pb(v);
			vec[v].pb(u);
		}
		void calculateDistance(int source, vector<int>& dist)
		{
			vector<bool> visited(SZ(vec), false);
			dist[source] = 0;
			queue<int> Q;
			queue<int> di;
			Q.push(source);
			di.push(-1);
			while (!Q.empty())
			{
				int top = Q.front();
				int d = di.front();
				Q.pop();
				di.pop();
				if (!visited[top])
				{
					visited[top] = true;
					dist[top] = d + 1;
					for(int nei : vec[top])
					{
						if (!visited[nei])
						{
							Q.push(nei);
							di.push(dist[top]);
						}
					}
				}
			}
		}

		bool dfs(int source, vector<bool>& visited, int parent, vector<int>& cyc, int& start)
		{
			if (!visited[source])
			{
				visited[source] = true;
				for(int nei : vec[source])
				{
					if (nei == parent) continue;
					cyc.push_back(nei);
					bool val = dfs(nei, visited, source, cyc, start);
					if (val == false)
						cyc.pop_back();
					else
						return val;
				}
				return false;
			}
			else if (source == start)
			{
				cycle = true;
				return cycle;
			}
			return false;
		}
};
int main()
{
	ios :: sync_with_stdio(false);
	int n; cin >> n;
	Graph g(n);
	REP(i,n)
	{
		int u,v; cin >> u >> v;
		g.addEdge(u-1,v-1);
	}
	vector<bool> visited(n, false);
	vector<int> vec;
	for(int i = 0; i < n; i++)
	{
		vec.clear();
		fill(all(visited), false);
		g.dfs(i, visited, -1, vec, i);
		if (g.cycle == true)
			break;
	}
	if (g.cycle)
	{
		vector<vector<int> > dist(n, vector<int>(n,0));
		for(int i = 0; i < n; i++)
			g.calculateDistance(i, dist[i]);
		for(int i = 0; i < n; i++)
		{
			if (find(all(vec), i) != vec.end())
				cout << "0 ";
			else
			{
				int ans = 0;
				for(int nei : vec)
				{
					if (!ans || dist[i][nei] < ans)
						ans = dist[i][nei];
				}
				cout << ans << " ";
			}
		}
		cout << "\n";
	}
}
