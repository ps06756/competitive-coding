/* Shows how to find distinct elements in a range */
#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

template<typename T>
class SegmentTree
{
	public:
		vector<T> tree;
		vector<T> arr;
		int64_t size;
		SegmentTree(i6 n)
		{
			size = n;
			i6 s = (1 << ((i6)ceil(log2(n))+1));
			tree = vector<T>(s);
			arr = vector<T>(s);
		}
		void constructSegmentTree(int node, int start, int end)
		{
			if (start == end)
				tree[node] = arr[start];
			else
			{
				int mid = start + (end - start)/2;
				constructSegmentTree(2*node + 1, start, mid);
				constructSegmentTree(2*node + 2, mid + 1, end);
				tree[node] = (tree[2*node + 1]^tree[2*node + 2]);
			}
		}
		void update(int l, T value)
		{
			update(0, size-1, 0, l, value);
		}
		void update(int start, int end, int index, int l,T value)
		{
			if (start == end)
			{
				tree[index] = value;
			}
			else
			{
				int mid = start + (end - start)/2;
				if (start <= l && l <= mid)
					update(start, mid, 2*index + 1, l, value);
				else if (mid + 1 <=l && l <= end)
					update(mid + 1, end, 2*index + 2, l, value);
				tree[index] = (tree[2*index + 1]^tree[2*index + 2]);
			}

		}
		T getQuery(int l, int r)
		{
			return getQuery(0, size-1, 0, l, r);
		}
		T getQuery(int start, int end, int index, int l, int r)
		{
			if (r < start || l > end || start > end)
				return (0);
			if (l <= start && end <= r)
				return tree[index];
			int mid = start + (end - start)/2;
			T p1 = getQuery(start, mid, 2*index + 1, l, r);
			T p2 = getQuery(mid + 1, end, 2*index + 2,l,r);
			return (p1^p2);
		}
};

class node
{
	public:
		int index,l,r, ans;
};

class compare
{
	public:
	bool operator () (node& n1, node& n2)
	{
		if (n1.r < n2.r) return true;
		else return false;
	}
};

class compare2
{
	public:
		bool operator () (node& n1, node& n2)
		{
			if (n1.index < n2.index) return true;
			else
				return false;
		}
};

int main()
{
	ios :: sync_with_stdio(false);
	cin.tie(NULL);
	int n; cin >> n;
	SegmentTree<int> tree1(n), tree2(n);
	vector<int> xorVal(n + 1,0);
	REP(i,n)
	{
		cin >> tree1.arr[i];
		xorVal[i + 1] = xorVal[i]^tree1.arr[i];
	}
	tree2.constructSegmentTree(0,0,n-1);
	map<int,int> last;
	int q; cin >> q;
	vector<node> vec(q);
	REP(i,q)
	{
		vec[i].index =  i;
		cin >> vec[i].l >> vec[i].r;
		vec[i].l--; vec[i].r--;
	}
	int used = 0;
	sort(all(vec), compare());
	last.clear();
	REP(i,q)
	{
		for(int j = used; j <= vec[i].r; j++)
		{
			int prevIndex = -1;
			if (last.find(tree1.arr[j]) != last.end())
				prevIndex = last[tree1.arr[j]];
			last[tree1.arr[j]] = j;
			if (prevIndex >= 0)
				tree2.update(prevIndex,0);
			tree2.update(j, tree1.arr[j]);
		}
		used = vec[i].r + 1;
		vec[i].ans = xorVal[vec[i].r+1]^xorVal[vec[i].l]^tree2.getQuery(vec[i].l,vec[i].r);
	}
	sort(all(vec), compare2());;
	for(int i = 0; i < SZ(vec); i++)
		cout << vec[i].ans << "\n";
	return (0);
}
