/* Excellent use of Floyd - Warshall algorithm to solve the problem. */
#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class Graph
{
	public:
		vector<vector<int> > vec;
		Graph(int n)
		{
			vec = vector<vector<int> > (n, vector<int>(n,0));
		}
		void removeVertex(int source)
		{
			for(int i = 0; i < SZ(vec); i++)
				vec[i][source] = vec[source][i] = 0;
		}
};

int main()
{
	ios :: sync_with_stdio(false);
	int n; cin >> n;
	Graph g(n);
	vector<vector<int> > vec(n, vector<int>(n,0)), dp(n, vector<int>(n,0));
	REP(i,n)
	{
		REP(j,n)
			cin >> vec[i][j];
	}
	vector<int> mp(n,0);
	int size = n-1;
	REP(i,n)
	{
		int temp; cin >> temp;
		temp--;
		mp[temp] = size--;
	}
	REP(i,n)
	{
		REP(j,n)
			g.vec[mp[i]][mp[j]] = vec[i][j];
	}
	vector<i6> ans;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
			dp[i][j] = g.vec[i][j];
	}
	for(int k = 0; k < n; k++)
	{
		i6 temp = 0;
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				if (!dp[i][j] || dp[i][j] > dp[i][k] + dp[k][j])
					dp[i][j] = dp[i][k] + dp[k][j];
				if (i != j && i <= k && j <= k)
					temp += dp[i][j];
			}
		}
		ans.pb(temp);
	}
	reverse(all(ans));
	for(i6 num : ans)
		cout << num << " ";
	cout << "\n";
	return (0);
}
