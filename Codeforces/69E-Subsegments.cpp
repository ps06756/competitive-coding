#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	ios :: sync_with_stdio(false);
	i6 n,k; cin >> n >> k;
	vector<i6> vec(n);
	map<i6,i6, greater<i6> > mp;
	REP(i,n)
		cin >> vec[i];
	for(i6 i = 0; i < k; i++)
		mp[vec[i]]++;
	set<i6, greater<i6> > st;
	for(auto it = mp.begin(); it != mp.end(); it++)
	{
		if (it->second == 1)
			st.insert(it->first);
	}
	if (!st.empty())
		cout << *st.begin() << "\n";
	else
		cout << "Nothing\n";
	for(int i = 1; i + k - 1 < n; i++)
	{
		i6 prev = vec[i-1], next = vec[i + k - 1];
		mp[prev]--;
		mp[next]++;
		if (mp[prev] != 1 && st.count(prev) == 1)
			st.erase(st.find(prev));
		if (mp[next] != 1 && st.count(next) == 1)
			st.erase(st.find(next));
		if (mp[prev] == 1)
			st.insert(prev);
		if (mp[next] == 1)
			st.insert(next);
		if (!st.empty())
			cout << *st.begin() << "\n";
		else 
			cout << "Nothing\n";
	}
	return (0);
}
