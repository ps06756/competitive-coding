#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) output<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { output<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

pair<int,int> operator + (pair<int,int>& p1, pair<int,int>& p2)
{
	return make_pair(p1.first + p2.first, p1.second + p2.second);
}
vector<pair<int,int> > positions = { {1,0},{-1,0}, {0,1}, {0,-1} };

int main()
{
	ifstream input("input.txt");
	ofstream output("output.txt");
	int n,m, k; input >> n >> m;
	input >> k;
	queue<pair<int, int > > Q;
	REP(i,k)
	{
		pair<int,int> temp;
		input >> temp.xx >> temp.yy;
		temp.xx--; temp.yy--;
		Q.push(temp);
	}
	vector<vector<bool> >visited(n, vector<bool>(m,false));
	pair<int,int> last = {0,0};
	while (!Q.empty())
	{
		pair<int,int> item = Q.front(); Q.pop();
		if (!visited[item.xx][item.yy])
		{
			last = item;
			visited[item.xx][item.yy] = true;
			for(pair<int,int> j : positions)
			{
				pair<int,int> pos = item + j;
				if (pos.xx >= 0 && pos.xx < n && pos.yy >=0 && pos.yy < m && visited[pos.xx][pos.yy] == false)
					Q.push(pos);
			}
		}
	}
	output << last.xx + 1 << " " << last.yy + 1 << "\n";
	return (0);
}

