#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	string str; cin >> str;
	vector<bool> prime(SZ(str) + 1,true);
	vector<int> primes;
	map<char, int> charCount;
	for(int i = 0; i < SZ(str); i++)
		charCount[str[i]]++;
	for(int i = 2; i < SZ(prime); i++)
	{
		if (!prime[i]) continue;
		primes.push_back(i);
		for(int j = i; j < SZ(str); j += i)
			prime[j] = false;
	}
	vector<int> visited(SZ(str)+1, -1);
	visited[1] = 0;
	int cnt = 1;
	for(int p : primes)
	{
		int color = cnt;
		for(int i = p; i <= SZ(str); i += p)
		{
			if (visited[i] != -1)
			{
				color = visited[i];
				break;
			}
		}
		for(int i = p; i <= SZ(str); i += p)
			visited[i] = color;
		if (color == cnt)
			cnt++;
	}
	priority_queue<pair<int,char> > Q;
	for(auto it = charCount.begin(); it != charCount.end(); it++)
		Q.push(make_pair(it->second, it->first));
	vector<vector<int> > positions(cnt + 1, vector<int>());
	for(int i = 1; i < SZ(visited); i++)
		positions[visited[i]].push_back(i);
	sort(all(positions), [](vector<int>& v1, vector<int>& v2) { if (SZ(v1) > SZ(v2)) return true; else return false;});
	string answer(SZ(str) + 1, 'a');
	for(int i = 0; i < SZ(positions); i++)
	{
		pair<int,char> top = Q.top();
		int req = SZ(positions[i]);
		if (top.first >= req)
		{
			for(int j = 0 ; j < SZ(positions[i]); j++)
				answer[positions[i][j]] = top.second;
		}
		else
		{
			cout << "NO\n";
			return (0);
		}
		if (top.first - req > 0)
			Q.push(make_pair(top.first - req, top.second));
		Q.pop();
	}
	cout << "YES\n" << answer.substr(1) << "\n";
	return (0);
}
