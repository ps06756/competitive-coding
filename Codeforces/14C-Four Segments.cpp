#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

double checkDistance(pair<int,int> p1, pair<int,int> p2, pair<int,int> p3)
{
	double dist1 = sqrt((p2.xx - p1.xx)*(p2.xx - p1.xx) + (p2.yy - p1.yy)*(p2.yy - p1.yy));	
	pair<int,int> v1 = {p2.xx - p1.xx, p2.yy - p1.yy}, v2 = {p3.xx - p2.xx, p3.yy - p2.yy};
	if (v1.xx*v2.xx + v1.yy*v2.yy != 0)
	{
		cout << "NO\n";
		exit(0);
	}
	else if ((v1.yy == 0 && v2.xx == 0) || (v1.xx == 0 && v2.yy == 0))
		return dist1;
	else
	{
		cout << "NO\n";
		exit(0);
	}
}

int main()
{
	vector<pair<int,int> > points;
	int cnt1 = 0, cnt2 = 0;
	for(int i = 0; i <4; i++)
	{
		int x1,y1,x2,y2;
		cin >> x1 >> y1 >> x2 >> y2;
		pair<int,int> temp = make_pair(x2 - x1, y2 - y1);
		if (temp.xx == 0) cnt1++; else if (temp.yy == 0) cnt2++;
		points.pb(make_pair(x1,y1));
		points.pb(make_pair(x2,y2));
	}
	if (cnt1 != 2 || cnt2 != 2)
	{
		cout << "NO\n";
		exit(0);
	}
	sort(all(points));
	pair<int,int> origin = points[0];
	for(int i = 0; i < (int)points.size(); i++)
	{
		points[i].xx -= origin.xx;
		points[i].yy -= origin.yy;
	}
	int cnt = 0;
	vector<pair<int,int> > tempVec;
	map<pair<int,int>, bool> mp;
	for(int i = 0; i < SZ(points); i++)
	{
		if (mp.count(points[i]) == 0)
		{
			mp[points[i]] = true;
			tempVec.push_back(points[i]);
			cnt++;
		}
	}
	points = tempVec;
	set<double> dist;
	if (cnt != 4)
		cout << "NO\n";
	else
	{
		sort(all(points), [] (pair<int,int>& p1, pair<int,int>& p2) { if (p1.xx == 0 && p1.yy == 0) return true; if (p1.xx*p2.yy - p2.xx*p1.yy >= 0) return true; else return false;});
		for(int i = 0; i < SZ(points); i++)
		{
			double temp = checkDistance(points[i], points[(i+1)%4], points[(i+2)%4]);
			dist.insert(temp);
		}
		if (SZ(dist) > 2)
		{
			cout << "NO\n";
			return (0);
		}
		else
			cout << "YES\n";
	}
	return (0);
}

