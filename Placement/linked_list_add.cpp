#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class node
{
	public:
	node* next;
	int value;
	node(node* ptr, int val)
	{
		next = ptr; 
		value = val;
	}
};


void addToList(node* he, int value)
{
	while (he->next != NULL)
		he = he->next;
	he->next = new node(NULL, value);
}

void printList(node*);
void add(node* he1, node* he2)
{
	int carry = 0;
	node* head = new node(NULL,0), *ptr1 = he1->next, *ptr2 = he2->next;
	while (ptr1 != NULL || ptr2 != NULL)
	{
		int total = carry;
		if (ptr1 != NULL)
		{
			total += ptr1->value;
			ptr1 = ptr1->next;
		}
		if (ptr2 != NULL)
		{
			total += ptr2->value;
			ptr2 = ptr2->next;
		}
		carry = total/10;
		addToList(head, total%10);
	}
	printList(head);
};

void printList(node* he)
{
	string str;
	he = he->next;
	while (he != NULL)
	{
		str += string(1,he->value + '0');
		he = he->next;
	}
	reverse(all(str));
	cout << str << "\n";
}

int main()
{
	string str1, str2; cin >> str1 >> str2;
	node* he1 = new node(NULL,0), *he2 = new node(NULL,0);
	for(int i = SZ(str1) - 1; i >=0 ; i--)
		addToList(he1,str1[i] - '0');
	for(int i = SZ(str2) - 1; i >=0; i--)
		addToList(he2, str2[i] - '0');
	printList(he1); printList(he2);
	add(he1, he2);
	cout << "Added the numbers\n";
	return (0);
}
