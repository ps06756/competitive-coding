#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

void mergeSort(vector<int>& vec, int start, int end)
{
		if (end - start > 1)
		{
			int mid = start + (end - start)/2;
			mergeSort(vec, start, mid); mergeSort(vec, mid + 1, end);
			inplace_merge(vec.begin() + start, vec.begin() + mid + 1, vec.begin() + end + 1);
		}
		else if (end == start + 1)
		{
			if (vec[start] > vec[end])
				swap(vec[start], vec[end]);
		}
}


int main()
{
	int tc; cin >> tc; 
	while (tc--)
	{
		int n; cin >> n;
		vector<int> vec(n);
		REP(i,n) cin >> vec[i];
		mergeSort(vec, 0, vec.size() - 1);
		REP(i,n) cout << vec[i] << " ";
		cout << "\n";
	}
	return (0);
}
