#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

void inversions(vector<int>& vec, int start, int end, int& inv)
{
		if (end - start > 1)
		{
			int mid = start + (end - start)/2, size1 = mid - start + 1, size2 = end - mid;
			inversions(vec, start, mid, inv); inversions(vec, mid + 1, end, inv);
			vector<int> temp;
			int i = start, j = mid + 1;
			while ( i <= mid && j <= end)
			{
				if (vec[i] <= vec[j])
					temp.pb(vec[i++]);
				else
				{
					temp.pb(vec[j++]);
					inv += (size1 - (i - start)); 
				}
			}
			while (i <= mid)
				temp.pb(vec[i++]);
			while (j <= end)
				temp.pb(vec[j++]);
			for(i = 0 ; i < SZ(temp); i++)
				vec[start + i] = temp[i];
		}
		else if (end == start + 1)
		{
			if (vec[start] > vec[end])
			{
				swap(vec[start], vec[end]);
				inv++;
			}
		}
}

int main()
{
	vector<int> temp = {1,2};
	int inv = 0;
	inversions(temp,0, temp.size() - 1, inv);
	cout << inv << "\n";
	return (0);
}
