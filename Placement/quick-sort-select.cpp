#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int partition(vector<int>& vec, int start, int end)
{
	int i = start, pivot = vec[end];
	FOR(j,start, end)
	{
		if (vec[j] <= pivot)
		{
			swap(vec[i], vec[j]);
			i++;
		}
	}
	swap(vec[end], vec[i]);
	return i;
}

void quickSort(vector<int>& vec, int start, int end)
{
	if (start < end)
	{
		int piv1 = partition(vec, start, end);
		quickSort(vec, start, piv1-1); 
		quickSort(vec, piv1 + 1, end);
	}
}

int quickSelect(vector<int>& vec, int start, int end, int k)
{
	if (start <= end)
	{
		int pi = partition(vec, start, end);
		if (pi == k)
			return vec[pi];
		else if (k > pi)
			return quickSelect(vec, pi + 1, end, k);
		else
			return quickSelect(vec, start, pi - 1, k);
	}
	else
		return -1;
}

int main()
{
	int tc; cin >> tc;
	while (tc--)
	{
		int n; cin >> n;
		vector<int> vec(n);
		REP(i,n)
			cin >> vec[i];
		cout << quickSelect(vec, 0, vec.size() - 1, vec.size()/2) << "\n";
	}
	return (0);
}
