#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

int main()
{
	ios :: sync_with_stdio(false);
	cin.tie(NULL);
	int n; cin >> n;
	map<int,vector<int> >  vec;
	REP(i,n)
	{
		int u,v; cin >> u >> v;
		vec[u].pb(v);
		vec[v].pb(u);
	}
	int source = 0;
	for(auto it = vec.begin(); it != vec.end(); it++)
	{
		if (SZ(it->second) == 1)
			source = it->first;
	}
	cout << source << " ";
	int trans = 0;
	while (trans != n)
	{
		int dest = vec[source][0];
		cout << vec[source][0] << " ";
		vec[source].pop_back();
		vec[dest].erase(find(all(vec[dest]), source));
		source = dest;
		trans++;
	}
	cout << "\n";
	return (0);
}
