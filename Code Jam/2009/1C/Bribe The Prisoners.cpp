#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
map<pair<int,int>, i6 > mp;
vector<int> vec;

i6 solve(int start, int end)
{
	pair<int,int> p  = { start, end};
	if (mp.count(p) == 1)
		return mp[p];
	else
	{
		int ans = INT_MAX; bool flag = false;
		for(int i = 0; i < SZ(vec); i++)
		{
			int temp = 0;
			if (vec[i] >= start && vec[i] <= end)
			{
				flag = true;
				temp = (end - start) + solve(vec[i] + 1, end) + solve(start, vec[i]-1);
				ans = min(ans, temp);
			}
		}
		if (flag == true)
			mp[p] = ans;
		else
			mp[p] = 0;
		return mp[p];
	}
}



int main()
{
	int tc; cin >> tc; 
	FOR(i,1,tc+1)
	{
		mp.clear();
		int n, q; cin >> n >> q;
		vec = vector<int> (q);
		REP(i,q)
			cin >> vec[i];
		cout << "Case #"<<i<<": " <<solve(1,n) << "\n";
	}
	return (0);
}
