#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

int main()
{
	ios :: sync_with_stdio(false);
	vector<i6> fact(1000001, 1);
	for(i6 i = 2; i < SZ(fact); i++)
		fact[i] = (fact[i-1]*i)%MOD;
	i6 n,m; cin >> n >> m;
	i6 ans = 1;
	int i = n , j = 1;
	for(int d = n-1; d >= 1 - m; d--)
	{
		int cnt=j-(i-d)+1;
		ans*=fact[cnt];
		ans%=MOD;
		i--;
		j++;
		i=max(i,1);
		j=min(j,(int)m);
	}
	cout << ans << "\n";
	return (0);
}

