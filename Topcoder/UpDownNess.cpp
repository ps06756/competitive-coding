#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;
#define MOD 1000000007

class UpDownNess
{
	public:
		int count(int n, int k)
		{
			vector<vector<i6> > dp(max(n+1,4),vector<i6>(k+1,0));
			dp[2][0] = 2; dp[1][0] = 1;
			dp[3][1] = 2; dp[3][0] = 4;
			for(i6 i = 3; i < n; i++)
			{
				for(i6 j = 0; j <= k; j++)
				{
					if (dp[i][j] > 0)
					{
						for(i6 l = 0; l <= i; l++)
						{
							if (j + l*(i - l) < SZ(dp[0]))
							{
								dp[i+1][j + l*(i - l)] += dp[i][j];
								dp[i+1][j + l*(i - l)] %= MOD;
							}
						}
					}
				}
			}
			return dp[n][k];
		}
};


int main()
{
	i6 n,k; cin >> n >> k;
	UpDownNess up;
	cout << up.count(n,k) << "\n";
	return (0);
}
