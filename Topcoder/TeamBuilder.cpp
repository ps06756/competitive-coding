/* This problem involved using simple DFS */
/* SRM 184 Div2 Hard */
#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)

class TeamBuilder
{
	public:
		void dfs(int source, vector<bool>& visited, set<int>& st, vector<string>& vec)
		{
			if (!visited[source])
			{
				visited[source] = true;
				st.insert(source);	
				REP(i, SZ(vec[source]))
				{
					if (vec[source][i] == '1')
						dfs(i, visited, st, vec);
				}
			}
		}
		vector<int> specialLocations(vector<string> vec)
		{
			vector<set<int> > st(SZ(vec));
			vector<bool> visited(SZ(vec), false);
			REP(i, SZ(vec))
			{
				fill(all(visited), false);
				dfs(i, visited, st[i], vec);
			}
			int cnt1 = 0, cnt2 = 0;
			REP(i, SZ(vec))
			{
				if (SZ(st[i]) == SZ(vec))
					cnt1++;
				bool flag = true;
				REP(j, SZ(vec))
				{
					if (st[j].find(i) == st[j].end())
						flag = false;
					if (flag == false)
						break;
				}
				if (flag == true)
					cnt2++;
			}
			vector<int> ans = {cnt1, cnt2};
			return ans;
		}
};
