#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class TrySail
{
	public:
		int get(vector<int> vec)
		{
			int total = 0;
			for(int i = 0; i < SZ(vec); i++) total ^= vec[i];
			vector<vector<bool> > prev(256, vector<bool>(256, false)), curr(256, vector<bool>(256, false));
			prev[0][0] = true;
			for (int st : vec)
			{
				for(int i = 0; i < 256; i++)
					fill(all(curr[i]), false);
				for(int i = 0 ; i < 256; i++)
				{
					for(int j = 0; j < 256; j++)
					{
						if (prev[i][j] == true)
						{
							curr[i][j] = true;
							curr[i^st][j] = true;
							curr[i][j^st] = true;
						}
					}
				}
				for(int i = 0; i < 256; i++)
					copy(all(curr[i]), prev[i].begin());
			}
			int glob = 0;
			for(int i = 0; i < 256; i++)
			{
				for(int j = 0; j < 256; j++)
				{
					if (prev[i][j] == true)
					{
						glob = max(glob, (total^i^j) + i + j);
					}
				}
			}
			return glob;
		}
};
