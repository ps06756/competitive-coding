#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
typedef vector<int> vi;
typedef map<int,int> mii;
typedef set<int> si;
typedef pair<int,int> pii;
typedef vector<pair<int,int> > vpii;

class OrderOfOperations
{
	public:
		int minTime(vector<string> s)
		{
			int dest = 0;
			vector<int> vec;
			for(string& str : s)
			{	
				int temp = 0;
				for (int j = 0 ; j < SZ(str); j++)
				{
					if (str[j] == '1')
						temp |= (1 << j);
				}
				dest |= temp;
				vec.push_back(temp);
			}
			int n = (1 << SZ(s[0]));
			vector<int> dp(n+1, 10000000);
			dp[0] = 0;
			for (int msk = 0; msk < n; msk++)
			{
				for (int i = 0; i < SZ(vec); i++)
				{
					int temp = msk | vec[i];	
					int cnt =  __builtin_popcount(temp - msk);
					dp[temp] = min(dp[temp], dp[msk] + cnt*cnt);
				}
			}
			return dp[dest];
		}
};
