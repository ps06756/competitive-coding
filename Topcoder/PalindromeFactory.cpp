#include <bits/stdc++.h>
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define MAX 51
using namespace std;
int dp[MAX][MAX];

class PalindromeFactory
{
	public:
		int solve(int start, int end, string str)
		{
			if (start > end)
				return (0);
			if (dp[start][end] == -1)
			{
				int val1 = solve(start + 1, end - 1, str);
				if (str[start] != str[end])
					val1++;
				int val2 = solve(start + 1, end, str) + 1;
				int val3 = solve(start, end - 1, str) + 1;
				dp[start][end] = min(val1, min(val2, val3));
			}
			return dp[start][end];
		}
		int getEditDistance(string initial)
		{
			fill(&dp[0][0], &dp[MAX-1][MAX], -1);
			int ans = solve(0, SZ(initial) - 1, initial);
			for (int i = 0 ; i < SZ(initial); i++)
			{
				for (int j = i + 1; j < SZ(initial); j++)
				{
					fill(&dp[0][0], &dp[MAX-1][MAX], -1);
					string temp = initial;
					swap(temp[i], temp[j]);
					ans = min(ans, solve(0, SZ(temp)-1, temp) + 1);
				}
			}
			return ans;
		}
};


