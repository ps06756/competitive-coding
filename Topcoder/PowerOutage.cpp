/* This problem involved finding the longest branch in a graph */
/* TOPCODER SRM 144 Div2 HARD */
#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)


class PowerOutage
{
	public:
		int getLongest(int source,vector<vector<pair<int,int> > >& vec, vector<bool>& visited)
		{
			int ans = 0;
			if (!visited[source])
			{
				visited[source] = true;
				foreach(vec[source], it)
				{
					ans = max(ans , getLongest(it->first,vec, visited) + it->second);
				}
			}
			return (ans);
		}

		int estimateTimeOut(vector<int> from, vector<int> to, vector<int> length)
		{	
			int total = 0;
			REP(i, SZ(length))
				total += length[i];
			total *= 2;
			vector<bool> visited(51, false);
			vector<vector<pair<int,int> > > vec(51);
			REP(i, SZ(from))
				vec[from[i]].push_back(mp(to[i], length[i]));
			return total - getLongest(0,vec, visited);
		}
};


