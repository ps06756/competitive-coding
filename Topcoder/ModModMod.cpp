#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define mp(x,y) make_pair((x), (y))
#define mp3(x,y,z) make_pair((x), make_pair( (y), (z)))
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
#define xx first
#define yy second
#define clr clear()
#define var(x) cout<< #x << " = "<<x<<"\n";
#define print(x) for_each((x).begin(), (x).end(), [](auto n) { cout<<x<<" " })
typedef int32_t i3;
typedef int64_t i6;
typedef vector<i3> vi;
typedef pair<i3,i3> ii;
typedef vector<pair<i3,i3> > vii;

class ModModMod
{
	public:
		long findSum(vector<int> vec, int r)
		{
			vector<int> ans(r + 1,1);
			int n = SZ(vec);
			for(int i = 0;  i < n; i++)
			{
				for(int j = vec[i]; j <= r; j++)
					ans[j%vec[i]] += ans[j];
				r = min(r, vec[i] - 1);
			}
			int sum = 0;
			for(int i = 0; i <= r; i++)
				sum += (i*ans[i]);
			return sum;
		}
};
