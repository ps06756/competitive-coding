#include <bits/stdc++.h>
using namespace std;
#define all(c) (c).begin(), (c).end()
#define cnt(c, x) ((c).find(x) != (c).end())
#define pb push_back
#define FOR(i, a, n) for(int i = (a); i < (n); i++)
#define REP(i, n) for(int i = 0; i < (n); i++)
#define SZ(x) ((int) (x).size())
#define foreach(C, i) for(auto i = (C).begin(); i != (C).end(); i++)
typedef vector<int> vi;
typedef map<int,int> mii;
typedef set<int> si;
typedef pair<int,int> pii;
typedef vector<pair<int,int> > vpii;

class FiringEmployees
{
	public:
		int fire(vector<int> manager, vector<int> salary, vector<int> prod)
		{
			int ans = 0; 
			vector<int> profit(SZ(manager),0);
			for (int i = SZ(manager) - 1; i >= 0; i--)
			{
				profit[i] += prod[i] - salary[i];
				if (profit[i] < 0)
					profit[i] = 0;
				if (manager[i]  == 0)
					ans += profit[i];
				else
					profit[manager[i]-1] += profit[i];
			}
			return ans;
		}
};


